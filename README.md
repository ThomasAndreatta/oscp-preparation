# OSCP Training

In this repo i'll keep track of my training and notes - using Obsidian and the folder "vault" as vault -
on exploit, tactics and vulnerabilities.
This training is based on a list of suggested machine on the platform HackTheBox.
The writeup in this repo are all in clear, this are ALL retired machine.

This is a list of all the machine that i'll - or are already - complete, for the Linux or Windows only table
open the vault and check the "Machine list.md" file.

# All machine
|Os| Logo | Machine | Platform Difficulty | Status | Personal Difficulty |
|----|----------|-------------|-----------------|---|---:|
|Windows|![](https://www.hackthebox.eu/storage/avatars/52e077ae40899ab8b024afd51cb29b1c_thumb.png)|**[Blue](./machine/Blue/blue)**|**easy**|**completed**|1|
|Windows|![](https://www.hackthebox.eu/storage/avatars/60dc190c4c015cfe3a3aef9b5afca254_thumb.png)|**[Legacy](./machine/Legacy/legacy)**|**easy**|**completed**|1|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/52e077ae40899ab8b024afd51cb29b1c_thumb.png)|**[Lame](./machine/Lame/lame)**|**easy**|**completed**|1|
|Windows|![](https://www.hackthebox.eu/storage/avatars/59f03a24178dbb2bdc94968c201e21f8_thumb.png)|**[Jerry](./machine/Jerry/jerry)**|**easy**|**completed**|3|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/0f058b73659ca043de9f5240abd651ca_thumb.png)|**[Bashed](./machine/Bashed/bashed)**|**easy**|**completed**|3|
|Windows|![](https://www.hackthebox.eu/storage/avatars/bb09ffeaffe2f5220a1d591bb7b4f95e_thumb.png)|**[Optimum](./machine/Optimum/optimum)**|**easy**|**completed**|4|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/efef52a0fb63d9c8db0ab6e50cb6ac79_thumb.png)|**[Shocker](./machine/Shocker/shocker)**|**easy**|**completed**|3|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/a3d8bf97412b0b6247aac14695eff21c_thumb.png)|**[Sense](./machine/Sense/sense)**|**easy**|**completed**|3|
|Windows|![](https://www.hackthebox.eu/storage/avatars/0fb6455a29eb4f2682f04a780ce26cb1_thumb.png)|**[Devel](./machine/Devel/devel)**|**easy**|**completed**|5|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/995f465295b99869fce21ecadea4604c_thumb.png)|**[Beep](./machine/Beep/beep)**|**easy**|**completed**|4|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/344a8f99e8f7dddfed764f791e2731df_thumb.png)|**[Nibbles](./machine/Nibbles/nibbles)**|**easy**|**completed**|2|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/453800925395b3a5b14099e005fb5a77_thumb.png)|**[Poison](./machine/Poison/poison.md)**|**medium**|**completed**|4|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/61b3b6a4382f58a804e221062ced0bfb_thumb.png)|**[Valentine](./machine/Valentine/valentine.md)**|**easy**|**completed**|6|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/f77c75f363afe0d0a6eeccf6a8d8c252_thumb.png)|**[Cronos](./machine/Cronos/cronos)**|**medium**|**completed**|6|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/cfb87d43d2b47380fd0f3a3efb6a47ed_thumb.png)|**[Solidstate](./machine/Solidstate/solidstate.md)**|**medium**|**completed**|5|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/3a186834eba2b780dacdaadcc157d309_thumb.png)|**Sunday**|**easy**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/c74d84352f781ad5c7a4c35c0c3aa0ac_thumb.png)|**DevOops**|**medium ++**|uncompleted|-|
|Windows|![](https://www.hackthebox.eu/storage/avatars/5837ac5e28291146a9f2a8a015540c28_thumb.png)|**Active**|**easy ++**|uncompleted|-|
|Windows|![](https://www.hackthebox.eu/storage/avatars/a8d2ae87fbe6d1ccfe93522d74defb3a_thumb.png)|**Bastard**|**medium**|uncompleted|-|
|Windows|![](https://www.hackthebox.eu/storage/avatars/a7686636f7cebb477cb7b6d6ce729f2c_thumb.png)|**bounty**|**easy**|uncompleted|-|
|Windows|![](https://www.hackthebox.eu/storage/avatars/709059a710d3d6ff1ba32bf0729ecbb8_thumb.png)|**Jeeves**|**medium ++**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/72d59997b352994c628a2b58b022b1d0_thumb.png)|**Hawk**|**medium ++**|uncompleted|-|
|Windows|![](https://www.hackthebox.eu/storage/avatars/290efae8869a5416de9cdb6344062188_thumb.png)|**Silo**|**medium**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/923486e3c3741f202be2cb923a9028dc_thumb.png)|**Nineveh**|**medium**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/583c00a8217dd7a0da9682af44e297db_thumb.png)|**Tartarsauce**|**medium**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/11680a85eaa657026b92b815be86e366_thumb.png)|**Node**|**medium**|uncompleted|-|
|Windows|![](https://www.hackthebox.eu/storage/avatars/db80da1cf486718a9fed04d0013d26e0_thumb.png)|**Bart**|**medium ++**|uncompleted|-|
|Windows|![](https://www.hackthebox.eu/storage/avatars/72c8ccb9247de82b107cd045528e45a8_thumb.png)|**Tally**|**hard ++**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/f9106389d256a42cf41619e85fbd8f01_thumb.png)|**Falafel**|**hard ++**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/a4260d1d7e18e96bc307831754e80c13_thumb.png)|**Kotarak**|**hard**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/391b0f0a41832d21577824602f02b177_thumb.png)|**Brainfuck**|**insane**|uncompleted|-|
|Linux/Others|![](https://www.hackthebox.eu/storage/avatars/63cd9dfcb485cbe8bb111108141ea23c_thumb.png)|**Jail**|**insane ++**|uncompleted|-|
<br></br>
