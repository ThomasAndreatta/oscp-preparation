# Web
* if the website looks useless try a different vhost, maybe the machine name. In any case, enumerate the hostname, ALL OF THEM.
* login panel without credential try sqlmap(sometimes the saving the request with burp and using it in sqlmap help) and sqlinjection, maybe with admin as username and commenting the other part after the username
* if you can't find nothing with your standard tools use dirbuster with his wordlists, they're bigger.
* If you need password try to use the name of the software (nibbleblog => nibble or nibbles)