* if you have a list of credentials found here and there put them all in a file and try ssh with [hydra](hydra.md#hydra)
	```bash	
	hydra -L users.txt -P pass.txt ssh://10.10.10.7
	```
* if the website looks useless try a different vhost, maybe the machine name. In any case, enumerate the hostname, ALL OF THEM.
* login panel without credential try sqlmap(sometimes the saving the request with burp and using it in sqlmap help) and sqlinjection, maybe with admin as username and commenting the other part after the username
* mysql db, simply use [mysqldump](machine/Cronos/cronos.md)

#mysql