#!/usr/bin/bash
a=1
name=solidstate
for i in *.png; do
  new=$(printf "%03d.png" "$a") #04 pad to length of 4
  mv -i -- "$i" "$1-$new"
  let a=a+1
done
