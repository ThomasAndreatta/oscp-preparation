# Beep - Full nmap
**PORT      STATE SERVICE    VERSION**
```txt
22/tcp    open  ssh        OpenSSH 4.3 (protocol 2.0)
| ssh-hostkey: 
|   1024 ad:ee:5a:bb:69:37:fb:27:af:b8:30:72:a0:f9:6f:53 (DSA)
|_  2048 bc:c6:73:59:13:a1:8a:4b:55:07:50:f6:65:1d:6d:0d (RSA)
25/tcp    open  smtp       Postfix smtpd
|_smtp-commands: beep.localdomain, PIPELINING, SIZE 10240000, VRFY, ETRN, ENHANCEDSTATUSCODES, 8BITMIME, DSN, 
80/tcp    open  http       Apache httpd 2.2.3
|_http-server-header: Apache/2.2.3 (CentOS)
|_http-title: Did not follow redirect to https://10.10.10.7/
|_https-redirect: ERROR: Script execution failed (use -d to debug)
110/tcp   open  pop3       Cyrus pop3d 2.3.7-Invoca-RPM-2.3.7-7.el5_6.4
|_pop3-capabilities: PIPELINING AUTH-RESP-CODE IMPLEMENTATION(Cyrus POP3 server v2) APOP EXPIRE(NEVER) STLS TOP RESP-CODES LOGIN-DELAY(0) USER UIDL
111/tcp   open  rpcbind    2 (RPC #100000)
143/tcp   open  imap       Cyrus imapd 2.3.7-Invoca-RPM-2.3.7-7.el5_6.4
|_imap-capabilities: ID CONDSTORE ATOMIC OK MAILBOX-REFERRALS NO UIDPLUS X-NETSCAPE LIST-SUBSCRIBED IMAP4rev1 LISTEXT IMAP4 QUOTA SORT IDLE URLAUTHA0001 THREAD=REFERENCES THREAD=ORDEREDSUBJECT LITERAL+ SORT=MODSEQ NAMESPACE STARTTLS ANNOTATEMORE BINARY MULTIAPPEND RIGHTS=kxte Completed RENAME UNSELECT CHILDREN ACL CATENATE
443/tcp   open  ssl/https?
|_ssl-date: 2021-09-10T16:12:02+00:00; +8m07s from scanner time.
882/tcp   open  status     1 (RPC #100024)
993/tcp   open  ssl/imap   Cyrus imapd
|_imap-capabilities: CAPABILITY
995/tcp   open  pop3       Cyrus pop3d
3306/tcp  open  mysql      MySQL (unauthorized)
4190/tcp  open  sieve      Cyrus timsieved 2.3.7-Invoca-RPM-2.3.7-7.el5_6.4 (included w/cyrus imap)
4445/tcp  open  upnotifyp?
4559/tcp  open  hylafax    HylaFAX 4.3.10
5038/tcp  open  asterisk   Asterisk Call Manager 1.1
10000/tcp open  http       MiniServ 1.570 (Webmin httpd)
```