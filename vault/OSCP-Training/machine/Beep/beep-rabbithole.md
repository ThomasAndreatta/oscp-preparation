# Port 10000 - RabbitHole

Switching to the [port 10000](https://10.10.10.7:10000/) we can find a different login panel:
![[beep2.png]]
From wikipedia:
> **Webmin** is a web-based system configuration tool for Unix-like systems, although recent versions can also be installed and run on Microsoft Windows.


[cgi](https://10.10.10.7:10000/password_change.cgi)
![[beep3.png]]
Looking at some code for this exploit looks like they need a username but we don't have any, let's go back and search for more stuff in the other ports.
