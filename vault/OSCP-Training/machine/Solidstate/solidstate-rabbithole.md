# Port 631 - RabbitHole

Now linpeas while listing all the possible vulnerabilities and info show the listening port too and from my experience if there's a listening port is worth to check it out.
![[solidstate-017.png]]

```
What is the service associated with port 631 TCP?

IPP uses TCP with port 631 as its well-known port. Products using the Internet Printing Protocol include CUPS (which is part of Apple macOS and many BSD and Linux distributions and is the reference implementation for most versions of IPP), Novell iPrint, and Microsoft Windows versions starting from MS Windows 2000.
```

The [CUPS website](https://ubuntu.com/server/docs/service-cups) tell us that there is a web interface
![[solidstate-019.png]]

Port forwarding the port 631 to our 4444
```bash
ssh -L 4444:127.0.0.1:631 mindy@10.10.10.51
```
We can access the cup webpage
![[solidstate-018.png]]

But this doesn't get us anywhere.