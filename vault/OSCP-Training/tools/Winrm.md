# Winrm
> evil-winrm
Connect to a windows machine with powershell(authenticated).

WinRM (Windows Remote Management) is the Microsoft implementation of WS-Management Protocol. 

Creds required, usually on port 5985

https://github.com/Hackplayers/evil-winrm
#tools #windows #reverseshell #powershell