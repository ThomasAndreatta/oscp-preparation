# Portforwarding
---
In computer networking, port forwarding or port mapping is an application of network address translation (NAT) that redirects a communication request from one address and port number combination to another while the packets are traversing a network gateway, such as a router or firewall

---
## Local Port Forwarding
>The ssh is listening.

Local port forwarding allows you to forward a port on the local (ssh client) machine to a port on the remote (ssh server) machine, which is then forwarded to a port on the destination machine.

The SSH client listens on a given port and tunnels any connection to that port to the specified port on the remote SSH server, which then connects to a port on the destination machine. 
```bash
ssh -L [LOCAL_IP:]LOCAL_PORT:DESTINATION:DESTINATION_PORT USER@SSH_SERVER
```

```bash
ssh -L 4444:127.0.0.1:80 charix@10.10.10.84
```
Going to http://localhost:4444 you'll be able to see the webserver of charix open only for his machine.
https://localhost:4444 will return https://othermachine:80


---
## Remote Port Forwarding
>You're listening

Remote port forwarding is the opposite of local port forwarding. It allows you to forward a port on the remote (ssh server) machine to a port on the local (ssh client) machine, which is then forwarded to a port on the destination machine.
```
ssh -R 8080:127.0.0.1:3000 -N -f user@remote.host
```

The command above will make the ssh server listen on port `8080`, and tunnel all traffic from this port to your local machine on port `3000`.

Now your fellow developer can type **the_ssh_server_ip:8080** in his/her browser and preview your awesome application.

---
# ssh
local:
```bash
ssh -L 4444:127.0.0.1:80 charix@10.10.10.84
```

remote:
```bash
ssh -R 8080:127.0.0.1:3000 -N -f user@remote.host
```


#portforwarding #tools #ssh