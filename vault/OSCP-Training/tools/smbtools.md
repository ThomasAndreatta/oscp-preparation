# SMB - Tools
---
# enum4linux
enum4linux -U <ip>

---

# smbclient
* -U set the username with we want to login, empty is default user
* -L list all the visible folder
	```bash
	smbclient  -L \\10.10.10.237 -U ""
	```
* With 4 \ connect in ftp style
	```bash
	smbclient \\\\10.10.10.40\\Users  -U ""
	```
--- 

# smbmap
smbmap -H <ip>

---
### MSF
> #windows7
>* [MS17-010: EternalBlue SMB Remote Windows Kernel Pool Corruption](https://www.rapid7.com/db/modules/exploit/windows/smb/ms17_010_eternalblue/)
>
>#windowsXP
>* [MS08-067: Microsoft Server Service Relative Path Stack Corruption](https://www.rapid7.com/db/modules/exploit/windows/smb/ms08_067_netapi/)
	
	
	
#linux #windows #smb #tools

	
	