# Portscanning
Here there's a list of all ports ever used/scanned by me with a small description, tag to vulnerability and possible exploit

# 21
> 21/tcp  open  ftp         vsftpd 2.3.4
>
>File Transfer Protocol

# 22
>22/tcp open  ssh         OpenSSH 4.7p1 Debian 
>
>Secure SHell
>Allows you to login into a different device via shell, upload and download file in and from folder that the user can access 


* sslyze

# 25
>25/tcp open smtp JAMES smtpd 2.3.2

# 53
> domain ISC BIND 9.10.3-P4
>
>DNS uses Port 53 which is nearly always open on systems, firewalls, and clients to transmit DNS queries. Rather than the more familiar Transmission Control Protocol (TCP) these queries use User Datagram Protocol (UDP) 


# 80
>80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu)) 
>80/tcp  open  http       lighttpd 1.4.35
>
>usually webserver 


# 110
>110/tcp open  pop3    JAMES pop3d 2.3.2
>
>Mailing port.

we can access to it with telnet, and login with
```
USER username
PASS password
#list all mail with
LIST
#print the content of one of them
RETR <MAIL ID>
```

# 119
>119/tcp open  nntp    JAMES nntpd (posting ok)
>
>The **Network News Transfer Protocol** (NNTP) is an application protocol used for transporting Usenet news articles (netnews) between news servers, and for reading/posting articles by the end user client applications
# 135
>135/tcp   open  [msrpc](rpc.md) 
>
Microsoft Remote Procedure Call

# 139
>139/tcp open  netbios-ssn [Samba smbd 3.X - 4.X](smb.md)
>
Network Basic Input/Output System
used for sharing data and info between system inside the local network, lot's of bug and faults.

# 443
>443/tcp open  ssl/https
>
>usually used as https or secure http instead of classic 8080, nothing particular

* [Heartbleed](cve.md#heartbleed)
# 445
>445/tcp   open  [microsoft-ds](rpc.md)
>445/tcp open  netbios-ssn [Samba smbd 3.X - 4.X](smb.md)
>
SMB type port: providing shared access to files, printers, and whatnot.
biggest targets for hackers


# 3389
>3389/tcp closed ms-wbt-server


# 8080
>8080/tcp open  http    Apache Tomcat/Coyote JSP engine 1.1
>
>Tomcat provides a "pure Java" HTTP web server environment in which Java code can run. Lot's of exploit, there's even a web interface

# 4555
>4555/tcp open rsip JAMES Remote Administration Tool 2.3.2
>
>root:root as default creds we can modify password and then log into port 110

#overview